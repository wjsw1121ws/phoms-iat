package com.paic.phoms.iat.common.utils;

import com.paic.phoms.iat.common.ienum.DateFormatPatternProp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @description: 日期格式化
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/

public class DateUtils {

    /**
     * 将日期转为字符串
     *
     * @param date    日期
     * @param pattern 格式
     * @return string   字符串日期
     */
    public static String formatDateToString(Date date, String pattern) {
        String stringDate = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            stringDate = dateFormat.format(date);
            return stringDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringDate;
    }

    /**
     * @param stringDate 字符串日期
     * @param pattern    格式
     * @return date         日期
     */
    public static Date formatStringToDate(String stringDate, String pattern) {
        Date date = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
            date = dateFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 校验字符串是否可被格式化
     * yyyy-MM-dd格式
     * @param strDate 传入的字符串日期
     * @return boolean
     */
    public static Boolean checkStrDateFormatEnable(String strDate) {
        try {
            if (strDate.length() < 6) {
                return false;
            }
            Integer.parseInt(strDate.replaceAll("-",""));
            String[] str = strDate.split("-");
            if (Integer.parseInt(str[0])>DateUtils.getCurrentYear()||
                    Integer.parseInt(str[1])>12||
                    Integer.parseInt(str[2])>DateUtils.getMaxDayOfMonth()
            ){
                return false;
            }
            new SimpleDateFormat(DateFormatPatternProp.Y_M_D1).parse(strDate);
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    /**
     * 获取最近一个月
     *
     * @return date
     */
    public static Date getLatestMonth() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -1);
        return cal.getTime();
    }

    /**
     * 获取最近一个月
     *
     * @param date 要获取的日期
     * @return date     一个月之前的日期
     */
    public static Date getLatestMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -1);
        return cal.getTime();
    }

    /**
     * 获取当前年份
     * @return integer
     */
    public static Integer getCurrentYear(){
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }

    /**
     * 获取指定日期的年份
     * @param date  日期
     * @return integer
     */
    public static Integer getCurrentYear(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    /**
     * 获取当前月份
     *
     * @return Integer
     */
    public static Integer getCurrentMonth() {
        return Calendar.getInstance().get(Calendar.MONTH) + 1;
    }

    /**
     * 获取指定日期月份
     * @param date  日期
     * @return  integer
     */
    public static Integer getCurrentMonth(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH)+1;
    }

    /**
     * 获取当前最大月份
     * @return Integer
     */
    public static Integer getMaxDayOfMonth(){
        Calendar cal = Calendar.getInstance();
        cal.set(getCurrentYear(),getCurrentMonth()-1,1);
        return cal.getActualMaximum(Calendar.DATE);
    }

    /**
     * 获取某个日期的最大天数
     * @return Integer
     */
    public static Integer getMaxDayOfMonth(Date date){
        Calendar cal = Calendar.getInstance();
        cal.set(getCurrentYear(date),getCurrentMonth(date)-1,1);
        return cal.getActualMaximum(Calendar.DATE);
    }
}
