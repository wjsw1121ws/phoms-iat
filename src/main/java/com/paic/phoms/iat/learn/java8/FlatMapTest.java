package com.paic.phoms.iat.learn.java8;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class FlatMapTest {

    @Test
    public void flatMapTest(){
        List<String> items = Arrays.asList("A_A","B_B");
        List<String> collect = items.stream().flatMap(item -> {
            String[] split = item.split("_");
            return Arrays.stream(split);
        }).collect(Collectors.toList());
        System.out.println(collect);
    }
}
