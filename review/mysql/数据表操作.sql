-- 删除数据表
DROP TABLE IF EXISTS student;

-- 创建数据表
CREATE TABLE if not exists stu
(
    id_student BIGINT      NOT NULL AUTO_INCREMENT COMMENT '主键',
    name       VARCHAR(10) NOT NULL COMMENT '学生姓名',
    age        INT         NOT NULL COMMENT '学生年龄',
    type       INT(2)      NOT NULL COMMENT '学生性别, 0:表示男，1:表示女',
    descr      VARCHAR(50) NOT NULL COMMENT '学生描述',
    PRIMARY KEY (id_student)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = gb2312
  COLLATE = gb2312_chinese_ci COMMENT ='学生信息表';

-- 修改表名称
ALTER TABLE stu RENAME TO student;

-- 修改字符集
ALTER TABLE student
    CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_bin;

-- 修改字段
ALTER TABLE student
    CHANGE type gender CHAR;

-- 修改字段数据类型
ALTER TABLE student
    MODIFY gender TINYINT NOT NULL;

-- 删除字段
ALTER TABLE student
    DROP descr;

-- 添加字段
-- 添加到最后
ALTER TABLE student
    ADD descr VARCHAR(50) COMMENT '学生描述';
-- 添加到最前秒
ALTER TABLE student
    ADD stu_id BIGINT NOT NULL COMMENT '学号' FIRST;

-- 查看数据表结构
-- SQL的形式查看表结构
SHOW CREATE TABLE student;
-- 以表格的形式展示表结构
DESC student;
DESCRIBE student;
