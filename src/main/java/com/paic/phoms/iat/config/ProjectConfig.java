package com.paic.phoms.iat.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * @description: 项目配置
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/

@Data
@Component
@PropertySource("classpath:com/paic/phoms/iat/config/project.properties")
public class ProjectConfig {
    /**
     * 协议http/https
     */
    @Value("${phoms_iat.request.protocol}")
    private String protocol;
    /**
     * 域名/ip
     */
    @Value("${phoms_iat.request.host}")
    private String host;
    /**
     * 端口
     */
    @Value("${phoms_iat.request.port}")
    private Integer port;

    @Value("${phoms_iat.request.project.name}")
    private String projectName;

    /**
     * 下载文件目录
     */
    @Value("${phoms_iat.file.download.path}")
    private String downloadPath;
}
