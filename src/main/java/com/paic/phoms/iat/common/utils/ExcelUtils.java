package com.paic.phoms.iat.common.utils;

import cn.hutool.poi.excel.ExcelUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 获取工作簿对象,支持xls和xlsx
 *
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class ExcelUtils {
    private static final String EXCEL_EXTEND_XLS = ".xls";
    private static final String EXCEL_EXTEND_XLSX = ".xlsx";
    private static final String PATTERN_DECIMAL = "^[+-]?[\\d]+([.][\\d]+)?([Ee][+-]?[\\d]+)?$";
    private static final String PATTERN_MATCH_DATA_TYPE = "\\d{8}|\\d{10}";
    private static final String PATTERN_DATA_FORMAT = "yyyy-MM-dd";
    private static final String PATTERN_DOUBLE = "\\d+\\.0";
    Logger logger = LoggerFactory.getLogger(getClass());

    public Workbook getWb(String path) {
        //创建工作簿对象
        Workbook wb = null;
        //创建输入流
        FileInputStream inputStream = null;
        try {
            //文件名称为空
            if (StringUtils.isBlank(path)) {
                logger.info("======文件路径为空，请输入正确的文件名======");
                return null;
            }
            //获取文件扩展名
            String extendName = path.substring(path.lastIndexOf("."));
            //文件名不符合
            if (!EXCEL_EXTEND_XLS.equals(extendName) && !EXCEL_EXTEND_XLSX.equals(extendName)) {
                logger.info("======文件类型不正确，请使用xls或xlsx格式的Excel文件======");
                return null;
            }
            //判断要处理的文件类型
            inputStream = new FileInputStream(path);

            if (EXCEL_EXTEND_XLS.equals(extendName)) {
                wb = new HSSFWorkbook(inputStream);
            } else {
                wb = new XSSFWorkbook(inputStream);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return wb;
    }

    public JSONObject getDataFromExcel(String filePath) {
        Workbook wb = getWb(filePath);
        if (wb == null) {
            logger.info("======文件: " + filePath + "不存在======");
            return null;
        }
        Sheet sheet = wb.getSheetAt(0);
        if (sheet.getPhysicalNumberOfRows() == 0) {
            logger.info("=======" + sheet.getSheetName() + "中没有数据=======");
            return null;
        }
        JSONObject jsonObject = new JSONObject();
        int count = 0;
        try {
            for (int i = 0; i < sheet.getLastRowNum() + 1; i++) {
                Row row = sheet.getRow(i);
                if (row == null) {
                    continue;
                }
                List<Object> list = new ArrayList<>();
                for (int j = 0; j < row.getPhysicalNumberOfCells(); j++) {
                    Object value = getCellValue(row.getCell(j));
                    list.add(value);
                }
                if (list.get(list.size() - 1) != "") {
                    jsonObject.put(String.valueOf(count), list);
                    count++;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                wb.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    public List<String> getMergedData(String filePath) {
        Workbook wb = getWb(filePath);
        if (wb == null) {
            logger.info("======文件: " + filePath + "不存在======");
            return null;
        }
        Sheet sheet = wb.getSheetAt(1);
        if (sheet.getPhysicalNumberOfRows() == 0) {
            logger.info("=======" + sheet.getSheetName() + "中没有数据=======");
            return null;
        }
        List<String> list = null;
        try {
            list = new ArrayList<>();
            for (int i = 0; i < sheet.getLastRowNum() + 1; i++) {
                Row row = sheet.getRow(i);
                if (row == null) {
                    continue;
                }
                for (int j = 0; j < row.getPhysicalNumberOfCells(); j++) {
                    if (!isMergedRegion(sheet, i, j)) {
                        continue;
                    }
                    String regionValue = getMergedRegionValue(sheet, i, j);
                    if (list.contains(regionValue)) {
                        continue;
                    }
                    list.add(regionValue);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                wb.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public static Object getCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        Object obj = null;
        switch (cell.getCellType()) {
            case BLANK:
            case _NONE:
                obj = null;
                break;
            case BOOLEAN:
                obj = cell.getBooleanCellValue();
                break;
            case NUMERIC:
                obj = cell.getNumericCellValue();
                if (HSSFDateUtil.isCellDateFormatted(cell) && String.valueOf(obj).matches(PATTERN_MATCH_DATA_TYPE)) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN_DATA_FORMAT);
                    Date date = cell.getDateCellValue();
                    obj = dateFormat.format(date);
                }
                else if (String.valueOf(obj).matches(PATTERN_DECIMAL)) {
                    DecimalFormat df = new DecimalFormat("0");
                    obj = df.format(obj);
                }
                else if (String.valueOf(obj).matches(PATTERN_DOUBLE)) {
                    obj = String.valueOf(obj).replace(".0", "");
                }else {

                    obj = String.valueOf(obj);
                }
                break;
            case STRING:
                obj = cell.getStringCellValue();
                break;
            case FORMULA:
            default:
                break;
        }
        return obj;
    }

    public static Workbook getWorkbook(String filePath) {
        return new ExcelUtils().getWb(filePath);
    }

    public static JSONObject getExcelData(String filePath) {
        return new ExcelUtils().getDataFromExcel(filePath);
    }

    public static String getMergedRegionValue(Sheet sheet, int rowNum, int columnNum) {
        int mergedRegions = sheet.getNumMergedRegions();
        String result = null;
        for (int i = 0; i < mergedRegions; i++) {
            CellRangeAddress mergedRegion = sheet.getMergedRegion(i);
            int firstRow = mergedRegion.getFirstRow();
            int lastRow = mergedRegion.getLastRow();
            int firstColumn = mergedRegion.getFirstColumn();
            int lastColumn = mergedRegion.getLastColumn();
            if (rowNum >= firstRow && rowNum <= lastRow) {
                if (columnNum >= firstColumn && columnNum <= lastColumn) {
                    result = sheet.getRow(firstRow).getCell(firstColumn).getStringCellValue();
                }
            }
        }
        return result;
    }

    public static boolean isMergedRegion(Sheet sheet, int rowNum, int columnNum) {
        int mergedRegions = sheet.getNumMergedRegions();
        if (mergedRegions == 0) {
            return false;
        }
        for (int i = 0; i < mergedRegions; i++) {
            CellRangeAddress mergedRegion = sheet.getMergedRegion(i);
            int firstRow = mergedRegion.getFirstRow();
            int lastRow = mergedRegion.getLastRow();
            int firstColumn = mergedRegion.getFirstColumn();
            int lastColumn = mergedRegion.getLastColumn();
            if (rowNum >= firstRow && rowNum <= lastRow) {
                if (columnNum >= firstColumn && columnNum <= lastColumn) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        String path = "C:\\Users\\changchun_wu\\Desktop\\项目\\phoms-iat\\src\\main\\resources\\2020-09-21 16_12_17-用户数据.xlsx";
//        List<String> mergedData = new ExcelUtils().getMergedData(path);
//        System.out.println(mergedData);
        List<Map<String, Object>> maps = ExcelUtil.getReader(path).readAll();
        System.out.println(maps);
        System.out.println(getExcelData(path));
    }
}
