package com.paic.phoms.iat.common.ienum;

/**
 * @description: 接口协议
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class ProtocolProp {
    public static final String HTTP = "http";
    public static final String HTTPS = "https";
}
