package com.paic.phoms.iat.common.utils;

import com.paic.phoms.iat.common.ienum.SymbolProp;
import org.apache.commons.lang3.StringUtils;

import java.util.Random;
import java.util.UUID;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class IDUtils {

    /**
     * 生成
     * @return
     */
    public static String generate32UpperUUID (){
        return UUID.randomUUID().toString().replaceAll(SymbolProp.JOINER, StringUtils.EMPTY).toUpperCase();
    }
}
