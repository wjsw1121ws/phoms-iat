package com.paic.phoms.iat.common.ienum;

/**
 * @description: 正则工具类
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class RegexProp {
    /**
     * ip正则
     */
    public static final String IP_REGEX ="((25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)))\\.){3}(25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)))";
}
