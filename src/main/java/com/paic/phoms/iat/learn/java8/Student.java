package com.paic.phoms.iat.learn.java8;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/

@Data
@AllArgsConstructor
public class Student implements Serializable {
    private static final long serialVersionUID = 6279283232009473418L;

    private String name;
    private Integer age;
}
