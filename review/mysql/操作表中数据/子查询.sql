-- 查询已经选课的学生
SELECT *
FROM student s
WHERE s.stu_id IN (SELECT c.stu_id FROM course c);

-- 查询没有选课的学生
SELECT *
FROM student s
WHERE s.stu_id NOT IN (SELECT c.stu_id FROM course c);

-- 如果存在选修高数的同学那么就查询所有年龄大于18的学生
SELECT *
FROM student s
WHERE s.age > 18
  AND EXISTS(SELECT c.stu_id FROM course c WHERE course_id = 10002);

-- 查询学生姓名和所选的课程
SELECT s.name, (SELECT GROUP_CONCAT(c.name) FROM course c WHERE s.stu_id = c.stu_id GROUP BY s.stu_id)
FROM student s;