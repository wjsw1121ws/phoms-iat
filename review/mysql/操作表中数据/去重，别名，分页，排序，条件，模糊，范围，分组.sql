-- 无条件查询
SELECT *
FROM student;
SELECT stu_id, name, age, gender
FROM student s;

-- 去重查询-DISTINCT
SELECT DISTINCT age
FROM student s;

-- 别名-AS
-- 表设置别名
SELECT s.name
FROM student as s;
SELECT s.name
FROM student s;
-- 字段设置别名
SELECT name as stu_name1
FROM student;
SELECT name stu_name2
FROM student;

-- 限制条件查询(分页)-LIMIT，OFFSET
-- 不限制起始位置-从最上面读取2条记录
SELECT *
FROM student
LIMIT 2;
-- 限制起始位置-跳过前2条，读取后面的3条
SELECT *
FROM student
LIMIT 2,3;
-- 限制起始位置-跳过前2条，读取后面的3条
SELECT *
FROM student
LIMIT 3 OFFSET 2;

-- 排序-ORDER BY--ASC--DESC
-- 单一字段排序
SELECT *
FROM student
ORDER BY age DESC;
-- 多字段排序-先按年龄升序，再按性别降序
SELECT *
FROM student
ORDER BY age, gender DESC;

-- 条件查询--WHERE--AND
SELECT *
FROM student
WHERE age > 19
  AND gender = 0;

-- 模糊查询-LIKE
SELECT *
FROM student
WHERE name LIKE '%雪';

-- 范围查询-BETWEEN--AND
SELECT *
FROM student
WHERE age BETWEEN 18 AND 19;;

-- 分组--GROUP BY--GROUP_CONCAT
SELECT age, GROUP_CONCAT(name)
FROM student
GROUP BY age;

-- 分组过滤-- GROUP BY--HAVING
SELECT age, GROUP_CONCAT(name)
FROM student
GROUP BY age
HAVING COUNT(age) > 1;