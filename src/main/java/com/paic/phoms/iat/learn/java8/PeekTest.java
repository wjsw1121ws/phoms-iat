package com.paic.phoms.iat.learn.java8;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class PeekTest {

    @Test
    public void peekTest(){
        List<Student> items = new ArrayList<>();
        items.add(new Student("A",10));
        items.add(new Student("B",20));
        items.add(new Student("C",30));
        items.add(new Student("D",40));
        items.add(new Student("E",50));
        List<Student> collect = items.stream().peek(item -> item.setAge(100)).collect(Collectors.toList());
        System.out.println(collect);
    }
}
