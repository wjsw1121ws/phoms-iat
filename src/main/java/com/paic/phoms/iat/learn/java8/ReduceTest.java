package com.paic.phoms.iat.learn.java8;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class ReduceTest {
    @Test
    public void reduceTest(){
        List<Integer> items = Arrays.asList(1,2,3,4,5);
        Integer sum = items.stream().reduce(Integer::sum).get();
        System.out.println(sum);
        Integer sum1 = items.stream().reduce(5, Integer::sum);
        System.out.println(sum1);
        Integer reduce = items.stream().reduce(0, (x, y) -> {
            System.out.println("X: "+x+"\t"+"Y: "+y);
            return x - y;
        }, (x, y) -> {
            // 这里第三个参数不起作用
            return x * y;
        });
        System.out.println(reduce);

    }
}
