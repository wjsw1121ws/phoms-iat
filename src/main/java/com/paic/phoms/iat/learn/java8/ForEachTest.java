package com.paic.phoms.iat.learn.java8;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class ForEachTest {

    @Test
    public void testMapForEach() {
        Map<String, Integer> items = new HashMap<>();
        items.put("A", 10);
        items.put("B", 20);
        items.put("C", 30);
        items.put("D", 40);
        items.put("E", 50);
        items.put("F", 60);
        items.forEach((k, v) -> System.out.println("key: " + k + "\tvalue: " + v));
        items.forEach((k, v) -> {
            if ("A".equals(k)) {
                System.out.println(v);
            }
        });
    }

    @Test
    public void testListForEach() {
        List<String> items = Arrays.asList("A", "B", "C", "D", "E");
        items.forEach(System.out::println);
        items.forEach(item -> {
            if (!item.isEmpty()) {
                System.out.println(item.toLowerCase());
            }
        });
    }
}
