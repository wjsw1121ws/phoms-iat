package com.paic.phoms.iat.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @description: json工具类
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class JsonUtils<T> {

    /**
     * 将json对象变为对象
     *
     * @param data  json对象
     * @param clazz 要转换的class
     * @param <T>   要转换成的对象
     * @return T
     */
    public static <T> T jsonObjToPojo(JSONObject data, Class<T> clazz) {
        return JSONObject.toJavaObject(data, clazz);
    }

    /**
     * 将json对象变为list
     *
     * @param data  json对象
     * @param clazz 要转换的class
     * @param <T>   要转换成的对象
     * @return T
     */
    public static <T> List<T> jsonArrayToList(JSONArray data, Class<T> clazz) {
        if (data == null) {
            return null;
        }
        String jsonString = data.toJSONString();
        return JSONArray.parseArray(jsonString, clazz);
    }

    /**
     * 判断json对象是否为空
     *
     * @param data 对象
     * @return boolean
     */
    public static boolean isBlank(JSON data) {
        if (data instanceof JSONObject) {
            return CollectionUtils.isEmpty((JSONObject) data);
        }
        return CollectionUtils.isEmpty((JSONArray) data);
    }

    /**
     * 判断json对象是否为空
     *
     * @param data 对象
     * @return boolean
     */
    public static boolean isNotBlank(Object data) {
        if (data instanceof JSONObject) {
            return !CollectionUtils.isEmpty((JSONObject) data);
        }
        return !CollectionUtils.isEmpty((JSONArray) data);
    }
}
