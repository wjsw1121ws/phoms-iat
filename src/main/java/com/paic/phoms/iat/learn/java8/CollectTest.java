package com.paic.phoms.iat.learn.java8;

import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class CollectTest {
    Student s1 = new Student("A", 10);
    Student s2 = new Student("B", 20);
    Student s3 = new Student("C", 10);
    List<Student> list = Arrays.asList(s1, s2, s3);

    /**
     * 封装成list
     */
    @Test
    public void collectToListTest() {

        //装成list
        List<Integer> items = list.stream().map(Student::getAge).collect(Collectors.toList());
        System.out.println(items);
    }

    /**
     * 封装成set
     */
    @Test
    public void collectToSetTest() {
        //转成set
        Set<Integer> set = list.stream().map(Student::getAge).collect(Collectors.toSet());
        System.out.println(set);
    }

    /**
     * 封装成map
     */
    @Test
    public void collectToMapTest() {
        Map<String, Integer> map = list.stream().collect(Collectors.toMap(Student::getName, Student::getAge));
        System.out.println(map);
    }

    /**
     * 字符串连接
     */
    @Test
    public void collectJointTest() {
        String str = list.stream().map(Student::getName).collect(Collectors.joining(",", "(", ")"));
        System.out.println(str);
    }

    /**
     * 聚合操作
     */
    @Test
    public void collectPolymerizeTest() {
        // 计算总数
        Long count = list.stream().collect(Collectors.counting());
        System.out.println(count);
        // 最大值
        Integer max = list.stream().map(Student::getAge).collect(Collectors.maxBy(Integer::compareTo)).get();
        System.out.println(max);
        // 最小值
        Integer min = list.stream().map(Student::getAge).collect(Collectors.minBy(Integer::compareTo)).get();
        System.out.println(min);
        // 最大值
        Integer sum = list.stream().collect(Collectors.summingInt(Student::getAge));
        System.out.println(sum);
        // 平均数
        Double avg = list.stream().collect(Collectors.averagingDouble(Student::getAge));
        System.out.println(avg);
        // 统计
        DoubleSummaryStatistics summary = list.stream().collect(Collectors.summarizingDouble(Student::getAge));
        System.out.println(summary);
    }

    /**
     * 分组
     */
    @Test
    public void collectGroupTest() {
        // 按年龄分组
        Map<Integer, List<Student>> map = list.stream().collect(Collectors.groupingBy(Student::getAge));
        System.out.println(map);
        // 多重分组先按年龄分组再按名字分组
        Map<Integer, Map<String, List<Student>>> map1 = list.stream().collect(Collectors.groupingBy(Student::getAge, Collectors.groupingBy(Student::getName)));
        System.out.println(map1);
    }

    /**
     * 分区
     */
    @Test
    public void collectPartitionTest() {
        Map<Boolean, List<Student>> map = list.stream().collect(Collectors.partitioningBy(student -> student.getAge() > 10));
        System.out.println(map);
    }

    @Test
    public void collectReducingTest() {
        Integer sum = list.stream().map(Student::getAge).collect(Collectors.reducing(Integer::sum)).get();
        System.out.println(sum);
    }
}
