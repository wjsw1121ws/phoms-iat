package com.paic.phoms.iat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhomsIatApplication {

    public static void main(String[] args) {
        SpringApplication.run(PhomsIatApplication.class, args);
    }
}
