-- 创建数据库
CREATE DATABASE if not exists review
    DEFAULT CHARACTER SET utf8mb4
    DEFAULT COLLATE utf8mb4_bin;
-- 选择数据库
USE review;
-- 查看所有数据库
SHOW DATABASES;
-- 查看指定数据库
SHOW DATABASES LIKE 'review';
-- 修改数据库
ALTER DATABASE review
    DEFAULT CHARACTER SET gb2312
    DEFAULT COLLATE gb2312_chinese_ci;
-- 删除数据库
DROP DATABASE if exists review;
