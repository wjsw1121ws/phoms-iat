package com.paic.phoms.iat.common.httpclient;

import com.paic.phoms.iat.common.pojo.ParamVo;
import com.paic.phoms.iat.common.pojo.ResponseVo;

/**
 * @description: Http请求
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public interface BaseHttpRequest {

    /**
     * post请求
     *
     * @param paramVo 请求参数
     * @return ResponseVo
     */
    ResponseVo doPost(ParamVo paramVo);

    /**
     * get请求
     *
     * @param paramVo 请求参数
     * @return ResponseVo
     */
    ResponseVo doGet(ParamVo paramVo);
}
