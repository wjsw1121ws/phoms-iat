-- 创建数据表
CREATE TABLE if not exists student
(
    id_student BIGINT      NOT NULL AUTO_INCREMENT COMMENT '主键',
    stu_id     BIGINT      NOT NULL COMMENT '学号',
    name       VARCHAR(10) NOT NULL COMMENT '学生姓名',
    age        INT         NOT NULL COMMENT '学生年龄',
    gender     TINYINT     NOT NULL COMMENT '学生性别, 0:表示男，1:表示女',
    PRIMARY KEY (id_student)
) ENGINE = InnoDB
  AUTO_INCREMENT = 10000
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT ='学生信息表';

-- 创建数据
INSERT INTO student(stu_id, name, age, gender) VALUE (10000, '小明', 20, 0);
INSERT INTO student(stu_id, name, age, gender) VALUE (10001, '小红', 19, 1);
INSERT INTO student(stu_id, name, age, gender) VALUE (10002, '小雪', 19, 1);
INSERT INTO student(stu_id, name, age, gender) VALUE (10003, '小强', 20, 0);
INSERT INTO student(stu_id, name, age, gender) VALUE (10004, '小刚', 18, 0);

-- 创建数据表
CREATE TABLE if not exists student_temp
(
    id_student BIGINT      NOT NULL AUTO_INCREMENT COMMENT '主键',
    stu_id     BIGINT      NOT NULL COMMENT '学号',
    name       VARCHAR(10) NOT NULL COMMENT '学生姓名',
    age        INT         NOT NULL COMMENT '学生年龄',
    gender     TINYINT     NOT NULL COMMENT '学生性别, 0:表示男，1:表示女',
    PRIMARY KEY (id_student)
) ENGINE = InnoDB
  AUTO_INCREMENT = 10000
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT ='学生信息表';

CREATE TABLE if not exists course
(
    id_course BIGINT      NOT NULL AUTO_INCREMENT COMMENT '主键',
    course_id BIGINT      NOT NULL COMMENT '学号',
    stu_id    BIGINT      NOT NULL COMMENT '学号',
    name      VARCHAR(10) NOT NULL COMMENT '课程名称',
    PRIMARY KEY (id_course)
) ENGINE = InnoDB
  AUTO_INCREMENT = 10000
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_bin COMMENT ='学生课程表';

INSERT INTO course(course_id, stu_id, name) VALUE (10001, 10001, '英语');
INSERT INTO course(course_id, stu_id, name) VALUE (10001, 10002, '英语');
INSERT INTO course(course_id, stu_id, name) VALUE (10001, 10003, '英语');
INSERT INTO course(course_id, stu_id, name) VALUE (10002, 10001, '高数');