package com.paic.phoms.iat.common.httpclient;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.paic.phoms.iat.common.ienum.NumberProp;
import com.paic.phoms.iat.common.pojo.FileUploadVo;
import com.paic.phoms.iat.common.ienum.ProtocolProp;
import com.paic.phoms.iat.common.ienum.SymbolProp;
import com.paic.phoms.iat.common.pojo.ParamVo;
import com.paic.phoms.iat.common.pojo.ResponseVo;
import com.paic.phoms.iat.common.utils.JsonUtils;
import com.paic.phoms.iat.config.ProjectConfig;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/

@Component
public class BaseHttpRequestImpl implements BaseHttpRequest {

    @Autowired
    private ProjectConfig projectConfig;

    @Override
    public ResponseVo doPost(ParamVo paramVo) {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost httpPost = new HttpPost(getRequestURL(paramVo));
        JSONObject jsonHeader = paramVo.getHeader();
        JSONObject jsonParam = paramVo.getParam();
        if (CollectionUtils.isEmpty(jsonHeader)) {
            jsonHeader.put(MIME.CONTENT_TYPE, MediaType.APPLICATION_JSON);
        } else if (jsonHeader.get(MIME.CONTENT_TYPE).toString().contains(MediaType.MULTIPART_FORM_DATA)) {
            try {
                handleMultipartReq(httpPost, jsonParam);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            handleApplicationJsonReq(httpPost, jsonHeader, jsonParam);
        }
        return getResponse(httpClient, httpPost);
    }

    @Override
    public ResponseVo doGet(ParamVo paramVo) {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(getRequestURI(paramVo));
        return getResponse(httpClient, httpGet);
    }

    /**
     * 处理application/json请求
     *
     * @param httpPost   post请求
     * @param jsonHeader 请求头
     * @param jsonParam  请求参数
     */
    private void handleApplicationJsonReq(HttpPost httpPost, JSONObject jsonHeader, JSONObject jsonParam) {
        jsonHeader.forEach((k, v) -> {
            httpPost.setHeader(k, v.toString());
        });
        if (!CollectionUtils.isEmpty(jsonParam)) {
            StringEntity stringEntity = new StringEntity(jsonParam.toJSONString(), StandardCharsets.UTF_8);
            httpPost.setEntity(stringEntity);
        }
    }

    /**
     * 处理multipart/form-data请求
     *
     * @param httpPost  post请求
     * @param jsonParam 请求参数
     */
    private void handleMultipartReq(HttpPost httpPost, JSONObject jsonParam) throws Exception {
        if (jsonParam.containsKey(FileUploadVo.FILE_UPLOAD)) {
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            String jsonData = jsonParam.get(FileUploadVo.FILE_UPLOAD).toString();
            FileUploadVo fileUploadVo = JSONObject.toJavaObject(JSON.parseObject(jsonData), FileUploadVo.class);
            if (!BeanUtil.hasNullField(fileUploadVo)) {
                ClassPathResource resource = new ClassPathResource(fileUploadVo.getFilepath());
                builder.addBinaryBody(fileUploadVo.getName(), resource.getFile(), ContentType.MULTIPART_FORM_DATA, fileUploadVo.getFilename());
                jsonParam.remove(FileUploadVo.FILE_UPLOAD);
            }
            jsonParam.forEach((k, v) -> {
                builder.addTextBody(k, v.toString(), ContentType.APPLICATION_JSON);
            });
            HttpEntity build = builder.build();
            httpPost.setEntity(build);
        }
    }

    /**
     * 获取响应
     *
     * @param httpClient httpClient
     * @param request    请求
     * @return ResponseVo
     */
    public ResponseVo getResponse(CloseableHttpClient httpClient, HttpRequestBase request) {
        ResponseVo responseVo = null;
        responseVo = new ResponseVo();
        try {
            CloseableHttpResponse response = httpClient.execute(request);
            responseVo.setStatusCode(responseVo.getStatusCode());
            if (response.getHeaders(MIME.CONTENT_DISPOSITION).length > NumberProp.ZERO) {
                String contentDesc = response.getHeaders(MIME.CONTENT_DISPOSITION)[NumberProp.ZERO].getValue();
                if (contentDesc.contains(FileUploadVo.FILE_NAME)) {
                    handleFileDownloadRep(response, responseVo, contentDesc);
                } else {
                    responseVo.setResponseBody(EntityUtils.toString(response.getEntity()));
                }
            } else {
                responseVo.setResponseBody(EntityUtils.toString(response.getEntity()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseVo;
    }

    /**
     * 处理文件下载请求
     *
     * @param response    响应结果
     * @param responseVo  响应对象
     * @param contentDesc 响应内容描述
     * @throws IOException 文件处理异常
     */
    private void handleFileDownloadRep(CloseableHttpResponse response, ResponseVo responseVo, String contentDesc) throws IOException {
        String filename = getFilenameFromContentDesc(contentDesc);
        saveResponseBodyToFile(response, responseVo, filename);
    }

    private void saveResponseBodyToFile(CloseableHttpResponse response, ResponseVo responseVo, String filename) throws IOException {
        String rootPath = System.getProperty("user.dir");
        String absoluteFilePath = rootPath +
                SymbolProp.DOUBLE_FORWARD_SLASH +
                projectConfig.getDownloadPath() +
                SymbolProp.DOUBLE_FORWARD_SLASH +
                filename;
        File file = new File(absoluteFilePath);
        if (!file.exists()) {
            file.createNewFile();
        }
        InputStream in = response.getEntity().getContent();
        FileOutputStream out = new FileOutputStream(file);
        IOUtils.copy(in, out);
        in.close();
        out.close();
        responseVo.setResponseFilePath(absoluteFilePath);
    }


    /**
     * 从内容描述中获取文件名
     * 格式: attachment; filename="11.xlsx"
     *
     * @param contentDesc 内容描述
     */
    private String getFilenameFromContentDesc(String contentDesc) {
        String[] contents = contentDesc.split(SymbolProp.SEMICOLON);
        for (String content : contents) {
            if (content.contains(FileUploadVo.FILE_NAME)) {
                return content.replaceAll(SymbolProp.DOUBLE_QUOTES, StringUtils.EMPTY).split(SymbolProp.EQUAL)[NumberProp.ONE].trim();
            }
        }
        return null;
    }


    /**
     * 将接口path转为URI
     *
     * @param paramVo 接口参数
     * @return URI
     */
    private URI getRequestURI(ParamVo paramVo) {
        URI uri = null;
        URIBuilder uriBuilder = null;
        try {
            if (StringUtils.isBlank(paramVo.getPath())) {
                throw new Exception("===接口不能为空===");
            }
            uriBuilder = new URIBuilder().setCharset(StandardCharsets.UTF_8);
            if (StringUtils.isBlank(projectConfig.getProtocol())) {
                uriBuilder.setScheme(ProtocolProp.HTTP);
            } else {
                uriBuilder.setScheme(projectConfig.getProtocol());
            }
            if (StringUtils.isBlank(projectConfig.getHost())) {
                throw new Exception("===请配置host(ip/域名)===");
            } else {
                uriBuilder.setHost(projectConfig.getHost());
            }
            if (projectConfig.getPort() != null) {
                uriBuilder.setPort(projectConfig.getPort());
            }
            uriBuilder.setPath(paramVo.getPath());
            if (JsonUtils.isNotBlank(paramVo.getParam())) {
                List<NameValuePair> listParams = new ArrayList<>();
                paramVo.getParam().forEach((k, v) -> {
                    listParams.add(new BasicNameValuePair(k, v.toString()));
                });

                uriBuilder.setParameters(listParams);
            }
            uri = uriBuilder.build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uri;
    }

    /**
     * 将接口path转为URL
     *
     * @param paramVo 请求参数
     * @return String
     */
    private String getRequestURL(ParamVo paramVo) {
        String url = null;
        try {
            if (StringUtils.isBlank(paramVo.getPath())) {
                throw new Exception("===接口不能为空===");
            }
            StringBuilder builder = new StringBuilder();
            if (StringUtils.isBlank(projectConfig.getProtocol())) {
                builder.append(ProtocolProp.HTTP);
            } else {
                builder.append(projectConfig.getProtocol());
            }
            builder.append(SymbolProp.COLON).append(SymbolProp.DOUBLE_BACKSLASH);
            if (StringUtils.isBlank(projectConfig.getHost())) {
                throw new Exception("===请配置host(ip/域名)===");
            } else {
                builder.append(projectConfig.getHost());
            }
            if (projectConfig.getPort() != null) {
                builder.append(SymbolProp.COLON).append(projectConfig.getPort());
            }
            builder.append(paramVo.getPath());
            url = builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return url;
    }
}
