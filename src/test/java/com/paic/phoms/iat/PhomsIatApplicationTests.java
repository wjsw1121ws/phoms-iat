package com.paic.phoms.iat;

import cn.hutool.core.codec.Base64Encoder;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.paic.phoms.iat.common.httpclient.BaseHttpRequest;
import com.paic.phoms.iat.common.ienum.SymbolProp;
import com.paic.phoms.iat.common.pojo.FileUploadVo;
import com.paic.phoms.iat.common.pojo.ParamVo;
import com.paic.phoms.iat.common.pojo.ResponseVo;
import com.paic.phoms.iat.common.utils.IDUtils;
import com.paic.phoms.iat.config.ProjectConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MIME;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Base64Utils;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.ws.rs.core.MediaType;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;


@RunWith(SpringRunner.class)
@SpringBootTest
class PhomsIatApplicationTests {

    @Resource(name = "redisTemplate")
    private RedisTemplate redisTemplate;

    @Resource(name = "stringRedisTemplate")
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private BaseHttpRequest baseHttpRequest;

    @Test
    void contextLoads(){
        String str = "{\n" +
                "      \"name\": \"1\",\n" +
                "      \"filename\": \"2\",\n" +
                "      \"fileLocation\": \"3\"\n" +
                "    }";
        JSON.parseObject(str);
        FileUploadVo fileUploadVo = JSON.toJavaObject(JSON.parseObject(str), FileUploadVo.class);
        System.out.println(fileUploadVo);
        String str1 = null;
        FileUploadVo fileUploadVo1 = JSONObject.toJavaObject(JSON.parseObject(str1), FileUploadVo.class);
        System.out.println(fileUploadVo1.getName());
    }

    @Test
    public void t(){
        String a = "attachment; filename=\"user.txt\"";
        System.out.println(a.indexOf("filename"));
        String filename = a.substring(a.indexOf("filename"));
        System.out.println(filename.split("=")[1].substring(1));
        System.out.println(a.substring(a.indexOf("filename")).split("=")[1].substring(1));
    }

    @Test
    public void t2() throws Exception{
        String publicKey = "AJOnAeTfeU4K+do5QdBM2BQUhfrRI2rYf/Gk4a3jZJB2ewekgq2VgLNislBdql/glA39w0NjXZyTg0mW917JdUlHqKoQ9765pJc4aTjvX+3IxdFhteyO2jE3vKX1GgA3i3n6+sMBAJiT3ax57i68mbT+KAeP1AX9199aj2W4JZeP";
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        byte[] res = Base64Utils.decode(publicKey.getBytes());
        X509EncodedKeySpec KeySpec = new X509EncodedKeySpec(res);
        RSAPublicKey pubKey = (RSAPublicKey)keyFactory.generatePublic(KeySpec);
        // here the exception occurs..
        Cipher cipher = Cipher.getInstance("RSA/None/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        byte[] cipherData = cipher.doFinal("Ab123456".getBytes());
        System.out.println(new String(cipherData));
    }

    @Test
    public void t3() throws Exception{
        String publicKey = "AJOnAeTfeU4K+do5QdBM2BQUhfrRI2rYf/Gk4a3jZJB2ewekgq2VgLNislBdql/glA39w0NjXZyTg0mW917JdUlHqKoQ9765pJc4aTjvX+3IxdFhteyO2jE3vKX1GgA3i3n6+sMBAJiT3ax57i68mbT+KAeP1AX9199aj2W4JZeP";
        BigInteger modulus = new BigInteger(publicKey, 16);
        BigInteger pubExp = new BigInteger("100001", 16);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        RSAPublicKeySpec pubKeySpec = new RSAPublicKeySpec(modulus, pubExp);
        RSAPublicKey key = (RSAPublicKey) keyFactory.generatePublic(pubKeySpec);

        Cipher cipher = Cipher.getInstance("RSA/ECB/NoPadding");
        cipher.init(Cipher.ENCRYPT_MODE, key);

        byte[] cipherData = cipher.doFinal("Ab123456".getBytes());
    }

    @Test
    public void test1(){
        Map<String,String> map = new HashMap<>();
        map.put("aa","bbb");
        redisTemplate.opsForHash().putAll("123",map);
        Map entries = redisTemplate.opsForHash().entries("123");
        System.out.println(entries);
    }
}
