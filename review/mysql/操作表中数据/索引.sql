-- 建表时创建普通索引
CREATE TABLE IF NOT EXISTS index_test
(
    id   BIGINT NOT NULL,
    name VARCHAR(50),
    INDEX index_test_id (id DESC)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4
  COLLATE utf8mb4_bin;

-- 建表时创建唯一索引
CREATE TABLE IF NOT EXISTS index_test
(
    id   BIGINT NOT NULL,
    name VARCHAR(50),
    UNIQUE INDEX index_test_id (id ASC)
) ENGINE = InnoDB
  DEFAULT CHARSET utf8mb4
  COLLATE utf8mb4_bin;

-- 为数据表添加索引
ALTER TABLE student ADD UNIQUE INDEX student_id_ix1 (id_student DESC);

-- 查看索引
SHOW INDEX FROM student;

-- 修改索引
ALTER TABLE student RENAME INDEX student_id_ix TO student_id_ix2;

-- 删除索引
-- 语法1
ALTER TABLE student DROP INDEX student_id_ix1;
-- 语法2
DROP INDEX student_id_ix2 ON student;

