-- 交叉链接--CROSS JOIN--WHERE--相当于获取两个表格的【笛卡尔积】然后找到符合WHERE条件的记录
SELECT *
FROM student s
         CROSS JOIN
     course c
where s.stu_id = c.stu_id;
-- ------------------------
SELECT *
FROM student s,
     course c
where s.stu_id = c.stu_id;

-- 内连接--INNER JOIN--ON---
SELECT *
FROM student s
         INNER JOIN course c ON s.stu_id = c.stu_id;

-- 外连接
-- 左外连接--LEFT OUTER JOIN
SELECT *
FROM student s
         LEFT OUTER JOIN course c ON s.stu_id = c.stu_id;
-- -----------------------
SELECT *
FROM student s
         LEFT JOIN course c ON s.stu_id = c.stu_id;
-- 右外连接--RIGHT OUTER JOIN
SELECT *
FROM student s
         RIGHT OUTER JOIN course c ON s.stu_id = c.stu_id;
-- -----------------------
SELECT *
FROM student s
         RIGHT JOIN course c ON s.stu_id = c.stu_id;

