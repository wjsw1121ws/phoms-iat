package com.paic.phoms.iat.learn.java8;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class MethodTest {

    @Test
    public void methodTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        System.out.println(list.stream().allMatch(e -> e > 10));
        System.out.println(list.stream().noneMatch(e -> e > 10));
        System.out.println(list.stream().anyMatch(e -> e > 4));
        System.out.println(list.stream().findFirst().get());
        System.out.println(list.stream().findAny().get());
        System.out.println(list.stream().count());
        System.out.println(list.stream().max(Integer::compareTo).get());
        System.out.println(list.stream().min(Integer::compareTo).get());
    }
    @Test
    public void streamMethodTest1() {
        List<String> list = Arrays.asList("A", "B", "C", "D", "E");
        System.out.println(list.stream().max(String::compareTo).get());
        System.out.println(list.stream().min(String::compareTo).get());
    }

}
