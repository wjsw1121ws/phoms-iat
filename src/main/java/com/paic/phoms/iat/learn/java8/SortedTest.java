package com.paic.phoms.iat.learn.java8;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class SortedTest {

    @Test
    public void sortedListTest() {
        List<String> items = Arrays.asList("B", "C", "A", "E", "D");
        List<String> collect = items.stream().sorted().collect(Collectors.toList());
        System.out.println(collect);
        List<Integer> items1 = Arrays.asList(3,2,5,1,4);
        List<Integer> collect1 = items1.stream().sorted().collect(Collectors.toList());
        System.out.println(collect1);
    }
}
