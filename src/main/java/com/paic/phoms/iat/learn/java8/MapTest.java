package com.paic.phoms.iat.learn.java8;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class MapTest {

    @Test
    public void mapTest(){
        List<String> items = Arrays.asList("A_A","B_B");
        List<String> collect = items.stream().map(item -> item.replaceAll("_", "")).collect(Collectors.toList());
        System.out.println(collect);
    }
}
