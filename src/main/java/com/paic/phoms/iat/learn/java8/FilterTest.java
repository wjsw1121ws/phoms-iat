package com.paic.phoms.iat.learn.java8;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description:
 * @author: changchun_wu
 * @version: 1.0
 * @blame: Test Team
 **/
public class FilterTest {

    @Test
    public void filterTest() {
        List<Integer> items = Arrays.asList(6, 4, 6, 7, 3, 9, 8, 10, 12, 14, 14);
        items.stream().filter(item -> item > 5).distinct().skip(2).limit(1).forEach(System.out::println);
    }

    @Test
    public void streamFilterCollectTest() {
        List<Integer> items = Arrays.asList(6, 4, 6, 7, 3, 9, 8, 10, 12, 14, 14);
        List<Integer> collect = items.stream().filter(item -> item > 5).distinct().skip(2).limit(1).collect(Collectors.toList());
        System.out.println(collect);
    }
}
