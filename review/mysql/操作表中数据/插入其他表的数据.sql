-- 批量插入
INSERT INTO student(stu_id, name, age, gender)
VALUES (10005, '小刘', 22, 0),
       (10006, '小王', 21, 0);

-- 将student数据表中的数据拷贝到student_temp
INSERT INTO student_temp(id_student, stu_id, name, age, gender)
SELECT id_student, stu_id, name, age, gender
FROM student;

-- 替换插入, 如果没有就插入，如果有就替换
REPLACE INTO student_temp(id_student, stu_id, name, age, gender)
SELECT id_student, stu_id, name, age, gender
FROM student;

-- 忽略插入，如果没有就插入，如果有就跳过
INSERT IGNORE INTO student_temp(id_student, stu_id, name, age, gender)
SELECT id_student, stu_id, name, age, gender
FROM student;


